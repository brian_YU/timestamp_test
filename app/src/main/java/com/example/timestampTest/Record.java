package com.example.timestampTest;


import androidx.room.Entity;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_table")
public class Record {
    @PrimaryKey
    public int mac;

    @ColumnInfo(name = "userName")
    public String name;

    @ColumnInfo(name = "userAge")
    public int age;

    @ColumnInfo(name = "updateTime")
    public long updateTime;
}

