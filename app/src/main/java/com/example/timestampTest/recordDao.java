package com.example.timestampTest;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface recordDao {
    @Query("SELECT * FROM user_table")
    public List<Record> getAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertUser(Record... record);

    @Update
    public void updateUser(Record... record);

    @Delete
    public void deleteUser(Record... record);
}
