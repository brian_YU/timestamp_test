package com.example.timestampTest;

import android.Manifest;
import android.app.AlertDialog;
import android.app.PendingIntent;

import android.app.ProgressDialog;

import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.argox.sdk.barcodeprinter.BarcodePrinter;
import com.argox.sdk.barcodeprinter.connection.usb.USBConnection;
import com.argox.sdk.barcodeprinter.emulation.pplb.PPLB;
import com.argox.sdk.barcodeprinter.emulation.pplb.PPLBFont;
import com.argox.sdk.barcodeprinter.emulation.pplb.PPLBMediaType;
import com.argox.sdk.barcodeprinter.emulation.pplb.PPLBOrient;
import com.argox.sdk.barcodeprinter.emulation.pplb.PPLBPrintMode;
import com.argox.sdk.barcodeprinter.emulation.pplb.PPLBQRCodeErrCorrect;
import com.argox.sdk.barcodeprinter.emulation.pplb.PPLBQRCodeModel;
import com.example.timestampTest.bluetoothProtocol.Command;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


public class TestActivity extends AppCompatActivity {

    private static final String TAG = "TestActivity";

    boolean isBound = false;

    public static bluetooth bluetoothService;

    BluetoothGattCharacteristic characteristic;

    public static String[] tv = new String[]{"0","0","0","-1","NA"};

    private ProgressDialog dialog;
    private Intent bluetoothIntent;

    String MAC = "";

    static TextView tv_light;
    static TextView tv_accelerometer;
    static TextView tv_temperature;
    TextView tv_touch1;
    static TextView tv_touch2;
    TextView mac_tv;
    TextView tv_version;
    static TextView tv_ntc;
    String serial = "";

    CheckBox charge_success, charge_fail, reset_success, reset_fail, color_p, color_b, sos_btn, vibrate, led, light;

    UsbDevice device = null;

    private Handler handler;
    private Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        tv[0] = "0";tv[1] = "0";tv[2] = "0";tv[3] = "0";tv[4] = "0";

        tv_light = findViewById(R.id.tv_light);
        tv_accelerometer = findViewById(R.id.tv_accelerometer);
        tv_temperature = findViewById(R.id.tv_temperature);
        //tv_touch1 = findViewById(R.id.tv_touch1);
        tv_touch2 = findViewById(R.id.tv_touch2);
        mac_tv = findViewById(R.id.mac_tv);
        tv_version = findViewById(R.id.tv_version);
        tv_ntc = findViewById(R.id.tv_ntc);
        charge_success = findViewById(R.id.charge_success);
        charge_fail = findViewById(R.id.charge_fail);
        reset_success = findViewById(R.id.reset_success);
        reset_fail = findViewById(R.id.reset_fail);
        color_b = findViewById(R.id.color_b);
        color_p = findViewById(R.id.color_p);
        sos_btn = findViewById(R.id.sosBtn);
        vibrate = findViewById(R.id.vibrate);
        led = findViewById(R.id.whiteLED);
        light = findViewById(R.id.light);

        Intent intent = getIntent();
        MAC = intent.getStringExtra("MAC").split("-")[1];


        bluetoothIntent = new Intent(this, bluetooth.class);
        startService(bluetoothIntent);
        bindService(bluetoothIntent,conn, Context.BIND_AUTO_CREATE);

        dialog = ProgressDialog.show(TestActivity.this, "",
                "連接中...", true);

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                Intent intent = new Intent(TestActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 60000);

        //bluetoothService.setMac(MAC);

        for(int i = 0; i < 6; i++){
            if(Integer.parseInt(MAC.split(":")[i], 16) < 100) serial += "0";
            if(Integer.parseInt(MAC.split(":")[i], 16) < 10) serial += "0";
            serial += Integer.parseInt(MAC.split(":")[i], 16);
        }

        mac_tv.setText(MAC+"\n"+serial);
        tv_version.setText("版本 : " + bluetooth.firmwareVersion);

        tv_light.setText("光照度 : "+ tv[0]);
        tv_accelerometer.setText("陀螺儀 : "+ tv[1]);
        tv_temperature.setText("溫度 : "+ tv[2]);
        //tv_touch1.setText("接觸感應1 : "+ tv[3]);
        tv_touch2.setText("接觸感應2 : "+ tv[4]);


//        handler = new Handler(Looper.getMainLooper());
////
////        runnable = new Runnable() {
////            @Override
////            public void run() {
////
////
////
////                handler.postDelayed(this, 1000);
////            }
////        };
////        handler.postDelayed(runnable, 0);

        this.registerReceiver(mConnReceiver,
                new IntentFilter("TEST_APP_SET_TEXT"));

        IntentFilter filter = new IntentFilter(TestActivity.class.getName());
        registerReceiver(usbReceiver, filter);

        getUSBDevice();

    }

    void bind(View view) {
        Intent startIntent = new Intent(TestActivity.this, bluetooth.class);
        startIntent.putExtra("command", "SENSOR");
        startService(startIntent);
    }

    public void btn_sensor(View view) {
//        bluetoothService.sendCommand(Command.REQ_NTC_AND_TOUCH_SENSOR, new byte[]{});
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable(){
//
//            @Override
//            public void run() {
//                characteristic = bluetoothService.getCharacteristic();
//                Log.d("Return", byteArr2Str(characteristic.getValue()));
//                tv_temperature.setText("溫度 : "+characteristic.getValue()[5]);
//                tv_touch2.setText("接觸感應2 : "+characteristic.getValue()[7]);
        bluetoothService.sendCommand(Command.SET_HEATER,new byte[]{1, 1, 0, 15});
        //bluetoothService.sendCommand(Command.REQ_LUX_VALUE, new byte[]{});
//            }}, 500);
//
//        handler.postDelayed(new Runnable(){
//
//            @Override
//            public void run() {
//                characteristic = bluetoothService.getCharacteristic();
//                tv_light.setText("光照度 : "+(characteristic.getValue()[5]<<8+characteristic.getValue()[6]));
//            }}, 1000);

//        handler.postDelayed(new Runnable(){
//
//            @Override
//            public void run() {
//                characteristic = bluetoothService.getCharacteristic();
//                Log.d("Return", byteArr2Str(characteristic.getValue()));
//                tv_temperature.setText("溫度 : "+characteristic.getValue()[5]);
//                tv_touch2.setText("接觸感應2 : "+characteristic.getValue()[7]+"");
//                bluetoothService.sendCommand(Command.REQ_LUX_VALUE, new byte[]{});
//            }}, 500);
    }

    public void btn_led(View view) {
        bluetoothService.sendCommand(Command.SET_RGB_TO_WHITE, new byte[]{1});
    }


    public void btn_vibration(View view) {
        bluetoothService.sendCommand(Command.SET_VIBRATE,new byte[]{0x01});
        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){

            @Override
            public void run() {
                Log.d("Return", byteArr2Str(bluetoothService.getCharacteristic().getValue()));
                bluetoothService.sendCommand(Command.SET_VIBRATE,new byte[]{0x00});
            }}, 2000);
    }

    public void btn_heat(View view) {
        bluetoothService.sendCommand(Command.SET_HEATER,new byte[]{1, 1, 0, 15});
    }

    public void btn_finish(View view) {

        if((charge_success.isChecked() || charge_fail.isChecked()) &&
                (reset_success.isChecked() || reset_fail.isChecked()) &&
                (color_p.isChecked() || color_b.isChecked())){
            if(bluetoothService.getConnectStatus())
                bluetoothService.sendCommand(Command.RELEASE_BONDING,new byte[]{});
            try {
                stopService(new Intent(TestActivity.this, bluetooth.class));
                Thread.sleep(2000);
                checkPass();
                //PPLBFunction();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }else{
            new AlertDialog.Builder(this)
                    .setTitle("未完成測試")
                    .setMessage("請確定已檢查柺杖顏色、充電和充電座重設功能")
                    .setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    void checkPass(){
        DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(TestActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };

        if(vibrate.isChecked() && sos_btn.isChecked() && led.isChecked() && light.isChecked() &&
                charge_success.isChecked() && reset_success.isChecked() &&
                (tv_accelerometer.getText().toString().split(" : ")[1].length() > 0) && (Integer.parseInt(tv_light.getText().toString().split(" : ")[1]) != 0) &&
                (Integer.parseInt(tv_temperature.getText().toString().split(" : ")[1].split("/")[0]) > 0) && (Integer.parseInt(tv_ntc.getText().toString().split(" : ")[1])>0) &&
                (Integer.parseInt(tv_touch2.getText().toString().split(" : ")[1])==1) && (tv_version.getText().toString().split(" : ")[1].length() > 0)){
            writeFile(true, serial);
            new AlertDialog.Builder(this)
                    .setTitle("完成測試")
                    .setMessage("檢驗合格")
                    .setNegativeButton(android.R.string.no, OkClick)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            PPLBFunction(MAC, serial);
        }else{
            writeFile(false, serial);
            new AlertDialog.Builder(this)
                    .setTitle("完成測試")
                    .setMessage("檢驗不合格")
                    .setNegativeButton(android.R.string.no, OkClick)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }


    public void setTv(String value, int i) {
        switch (i){
            case 0:
                tv_accelerometer.setText(value);
                break;
            case 1:
                tv_light.setText(value);
                break;
            case 2:
                tv_temperature.setText(value);
                break;
            case 3:
                tv_ntc.setText(value);
                break;
            case 4:
                tv_touch2.setText(value);
                break;
            case 5:
                tv_version.setText(value);
                break;
        }
    }

    private boolean isExternalStorageWritable() {
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            Log.d(TAG, "isExternalStorageWritable: YES");
            return true;
        }
        else {
            Log.d(TAG, "isExternalStorageWritable: NO");
            return false;
        }
    }

    private void writeFile(boolean pass, String serial) {
        Log.d("HAHA", "test");
        if(isExternalStorageWritable() && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            File root = Environment.getExternalStorageDirectory();//getExternalFilesDir("/");
            File dir = new File (root.getAbsolutePath()+"/QC report/");
            if(!dir.exists()) dir.mkdir();
            Log.i(TAG, "externalFileDir = "+dir+" "+dir.exists());
            File file = new File(dir, "STICKuTesTAfterAsm.csv");
            try {
                FileOutputStream f = new FileOutputStream(file, true);
                PrintWriter pw = new PrintWriter(f);
                pw.print(MAC+",");
                pw.print(tv_version.getText().toString().split(":")[1].replace(" ", "")+",");
                if(color_b.isChecked()) pw.print("g,");
                else pw.print("p,");
                pw.print(serial+",");
                SimpleDateFormat sDateFormat = new SimpleDateFormat("yyMMddHHmmss");
                String date = sDateFormat.format(new java.util.Date());
                pw.print(date+",");//((Long)(System.currentTimeMillis()/1000+28800)).toString());
                if(pass) pw.println("PASS,");
                else pw.println("FAIL,");
                pw.flush();
                pw.close();
                f.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            /*File file = new File(Environment.getExternalStorageDirectory(), "STICKuTesT.txt");
            Log.d("HAHA", "test");

            String tmp = bluetooth.MAC +"\n\r";

            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file, true);
                fileOutputStream.write(tmp.getBytes());
                fileOutputStream.close();
                Log.d(TAG, "writeFile: YES");
            }
            catch (IOException e) {
                Log.d(TAG, Objects.requireNonNull(e.getMessage()));
            }*/
        }
    }

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            isBound = true;
            bluetooth.LocalBinder myBinder = (bluetooth.LocalBinder)binder;
            bluetoothService = myBinder.getService();
            bluetoothService.setMac(MAC);
            Log.i(TAG, "ActivityA onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
            Log.i("DemoLog", "ActivityA onServiceDisconnected");
        }
    };

    public boolean checkPermission (String permission) {
        int check = ContextCompat.checkSelfPermission(this,permission);
        return (check == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        unbindService(conn);

        this.unregisterReceiver(mConnReceiver);
        this.unregisterReceiver(usbReceiver);
        stopService(bluetoothIntent);

        //handler.removeCallbacks(runnable);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private String byteArr2Str(byte[] data){
        String str = "";
        for(int i = 0; i < data.length; i++) str += (Integer.toHexString(data[i])+" ");
        return str.toUpperCase();
    }

    private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getIntExtra("ID", 0) != -99)
                setTv(intent.getStringExtra("Content"), intent.getIntExtra("ID", 0));
            else
                handler.removeCallbacksAndMessages(null);
                dialog.dismiss();
            Log.d("Broadcast", "received " + intent.getStringExtra("Content"));
        }
    };

    private boolean getUSBDevice() {
        device = (UsbDevice) getIntent().getParcelableExtra(UsbManager.EXTRA_DEVICE);

        if (device == null) {
            UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
            HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
            for (String key : deviceList.keySet()) {
                device = deviceList.get(key);
                if(device.getVendorId()==5732) {
                    break;
                }
                Log.d("grandroid", device.getDeviceName() + ", vendorID=" + device.getVendorId());
            }
        }
        if (device != null) {
            UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
            if (!manager.hasPermission(device)) {//權限判斷.
                //Notice:
                //If it is executed here, then the interface will open fail, because the device is no permission.
                //When you append the "android.hardware.usb.action.USB_DEVICE_ATTACHED" request in the XXXManifest.xml file,
                //then the system will ask for permission each time you plug in the USB.
                final PendingIntent mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(TestActivity.class.getName()), 0);
                manager.requestPermission(device, mPermissionIntent);
                return true;
            }
        }
        return false;
    }

    private void PPLBFunction(String mac, String number) {

        if (getUSBDevice()) {
            return;
        }
        BarcodePrinter<USBConnection, PPLB> printer = new BarcodePrinter<USBConnection, PPLB>();

        try {
            //if you use BluetoothConnection instead of TCPConnection , you must implements onActivityResult(...) callback function
            //with calling printer.getConnection().onActivityResult(...)
            printer.setConnection(new USBConnection(this, device));
            printer.setEmulation(new PPLB());

            printer.getConnection().open();
            //call methods that you want.
            //setting.
            printer.getEmulation().getSetUtil().setHardwareOption(PPLBMediaType.Thermal_Transfer_Media, PPLBPrintMode.Tear_Off, 0);
            printer.getEmulation().getSetUtil().setOrientation(false);
            printer.getEmulation().getSetUtil().setClearImageBuffer();

            String text = "STICKU-"+mac;

            printer.getEmulation().getBarcodeUtil().printQRCode(65-30,10, PPLBQRCodeModel.Model_2, 7, PPLBQRCodeErrCorrect.Standard
                    , text.getBytes());
            printer.getEmulation().getBarcodeUtil().printQRCode(319-30,10, PPLBQRCodeModel.Model_2, 7, PPLBQRCodeErrCorrect.Standard
                    , text.getBytes());
            printer.getEmulation().getBarcodeUtil().printQRCode(574-30,10, PPLBQRCodeModel.Model_2, 7, PPLBQRCodeErrCorrect.Standard
                    , text.getBytes());

            printer.getEmulation().getTextUtil().printText(64-30,215, PPLBOrient.Clockwise_0_Degrees, PPLBFont.Font_1, 1, 1, false, number.getBytes());
            printer.getEmulation().getTextUtil().printText(318-30,215, PPLBOrient.Clockwise_0_Degrees, PPLBFont.Font_1, 1, 1, false, number.getBytes());
            printer.getEmulation().getTextUtil().printText(573-30,215, PPLBOrient.Clockwise_0_Degrees, PPLBFont.Font_1, 1, 1, false, number.getBytes());

            //set print conditions.
            printer.getEmulation().getSetUtil().setPrintOut(1, 1);
            printer.getEmulation().printOut();
            printer.getConnection().close();
        } catch (Exception ex) {
            try {
                printer.getConnection().close();
            }
            catch (Exception e) {
            }
            finally {
                Toast.makeText(this, ex.toString(), Toast.LENGTH_LONG).show();
                Log.e("argox_demo", null, ex);
            }
        }
    }

    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TestActivity.class.getName().equals(action)) {
                synchronized (this) {
                    device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if(device != null) {
                            //call method to set up device communication
                            /* *
                            tvDeviceName.setText("DeviceName: " + device.getDeviceName());
                            tvDeviceId.setText("DeviceId: " + device.getDeviceId());
                            tvVendorId.setText("VendorId: " + device.getVendorId());
                            tvProductId.setText("ProductId: " + device.getProductId());
                            tvDeviceProtocol.setText("DeviceProtocol: " + device.getDeviceProtocol());
                            tvDeviceClass.setText("DeviceClass: " + device.getDeviceClass());
                            * */
                        }
                    }
                    else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
        }
    };
}
