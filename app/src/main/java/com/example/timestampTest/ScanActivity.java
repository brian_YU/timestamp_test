package com.example.timestampTest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import static com.journeyapps.barcodescanner.CaptureManager.resultIntent;

public class ScanActivity extends AppCompatActivity {
    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scan);

        handler = new Handler();

        barcodeScannerView = (DecoratedBarcodeView) findViewById(R.id.barcodeView);

        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        barcodeScannerView.decodeContinuous(callback);
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(final BarcodeResult result) {
            Log.d("Result",result.getText());
            if(result.getText().toUpperCase().split("-")[0].equals("STICKU")) {
                barcodeScannerView.pause();
                BeepManager beepManager = new BeepManager(ScanActivity.this);
                beepManager.playBeepSoundAndVibrate();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        returnResult(result);
                    }
                });
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }

    };

    void returnResult(BarcodeResult rawResult) {
        Intent intent = resultIntent(rawResult, "Camera");
        Log.d("Scan", "Qrcode yes");
        this.setResult(Activity.RESULT_OK, intent);
        closeAndFinish();
    }

    void closeAndFinish() {
        if(barcodeScannerView.getBarcodeView().isCameraClosed()) {
            finish();
        } else {
            barcodeScannerView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        capture.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

}

//public class ScanActivity extends AppCompatActivity {
//
//    private CameraSource mCameraSource;
//
//    private DecoratedBarcodeView barcodeScannerView;
//    private CaptureManager capture;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_scan);
//
//        //SurfaceView mSurfaceView = findViewById(R.id.qr_code_scanner_surface_view);
//
////        DisplayMetrics displayMetrics = new DisplayMetrics();
////        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
////        int height = (int) (displayMetrics.heightPixels * 0.385);
////        int width = (int) (displayMetrics.widthPixels * 0.67);
////
////        BarcodeDetector mBarcodeDetector = new BarcodeDetector.Builder(this)
////                .setBarcodeFormats(Barcode.QR_CODE).build();
////
////        mCameraSource = new CameraSource.Builder(this, mBarcodeDetector)
////                .setRequestedPreviewSize(height, width).setAutoFocusEnabled(true).build();
////
////        mSurfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
////            @Override
////            public void surfaceCreated(SurfaceHolder holder) {
////                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
////
////                    return;
////                }
////                try {
////                    mCameraSource.start(holder);
////                }
////                catch (IOException e) {
////                    Log.e("QRCodeScanner", Objects.requireNonNull(e.getMessage()));
////                }
////            }
////
////            @Override
////            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
////
////            }
////
////            @Override
////            public void surfaceDestroyed(SurfaceHolder holder) {
////
////            }
////        });
////
////        mBarcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
////            @Override
////            public void release() {
////
////            }
////
////            @Override
////            public void receiveDetections(Detector.Detections<Barcode> detections) {
////                final SparseArray<Barcode> qrCodes = detections.getDetectedItems();
////
////                if (qrCodes.size() != 0) {
////
////                    new Handler(Looper.getMainLooper()).post(new Runnable() {
////                        @Override
////                        public void run() {
////
////                            if(qrCodes.valueAt(0).displayValue.split("-")[0].equals("STICKU")) {
////                                Toast.makeText(ScanActivity.this, "Scanned: " + qrCodes.valueAt(0).displayValue.split("-")[1], Toast.LENGTH_LONG).show();
////
//////                                Intent startIntent = new Intent(ScanActivity.this, bluetooth.class);
//////                                startIntent.putExtra("command", "MACADDRESS");ire
//////                                startIntent.putExtra("MAC", qrCodes.valueAt(0).displayValue.split("-")[1]);
//////                                startService(startIntent);
////////                                bluetooth.setConnectStatus();
////
////                                mCameraSource.stop();
////                                Intent intent = new Intent(ScanActivity.this, TestActivity.class);
////                                intent.putExtra("MAC",  qrCodes.valueAt(0).displayValue);
////                                startActivity(intent);
////                                finish();
////                            }
////                        }
////                    });
////                }
////            }
////        });
////        barcodeScannerView = findViewById(R.id.barcodeView);
////
////        IntentIntegrator integrator = new IntentIntegrator(this);
////
////        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
////        integrator.setBeepEnabled(true);
////        integrator.setPrompt("Scan");
////        integrator.setCameraId(0);
////        integrator.setBeepEnabled(false);
////        integrator.setBarcodeImageEnabled(false);
////        integrator.initiateScan();
//
//        CaptureManager capture;
//        capture = new CaptureManager(this);
//        capture.initializeFromIntent(getIntent(), savedInstanceState);
//        capture.decode();
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data)
//    {
//        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
//        if (result!= null)
//        {
//            if (result.getContents()==null)
//            {
//                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_SHORT).show();
//            }
//            else
//            {
//                Toast.makeText(this,result.getContents(),Toast.LENGTH_SHORT).show();
//
//            }
//        }
//        else
//        {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mCameraSource.release();
//    }
//}
