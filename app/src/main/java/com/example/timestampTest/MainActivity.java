package com.example.timestampTest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "SplashActivity";

    private final String[] RequirePermission = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                databaseOperation();
//            }
//        }).start();
        init();
        permissionRequest();
    }

    private void permissionRequest(){
        ActivityCompat.requestPermissions(this,RequirePermission,1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permissionFlag = true;

        if (requestCode == 1) {
            for (int i = 0; i < grantResults.length; i++) {
                permissionFlag &= ActivityCompat.checkSelfPermission(this, permissions[i]) == PackageManager.PERMISSION_GRANTED;
            }
            if (permissionFlag) {

                //pageSelection();
                //stopService(new Intent(MainActivity .this, bluetooth.class));

//                Intent mainIntent;
//                mainIntent = new Intent(MainActivity.this, ScanActivity.class);
//
//                startActivity(mainIntent);
//                finish();

                new IntentIntegrator(this)
                        .setCaptureActivity(ScanActivity.class)
                        .setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
                        .setPrompt("請對準二維碼")
                        .setCameraId(0)
                        .setBeepEnabled(true)
                        .setBarcodeImageEnabled(true)
                        .initiateScan();
            }
            else {
                permissionRequest();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);
        if (result!= null)
        {
            if (result.getContents()==null)
            {
                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_SHORT).show();
                Log.d("LOG", "Content is null");
                finish();
            }
            else
            {
                //Toast.makeText(this,result.getContents(),Toast.LENGTH_SHORT).show();
                //Log.d("Result", result.getContents());
                Intent intent = new Intent(MainActivity.this, TestActivity.class);
                intent.putExtra("MAC",  result.getContents());
                startActivity(intent);
                finish();
            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
    }


    private void pageSelection () {

        int SPLASH_DISPLAY_LENGHT = 1000;

        new android.os.Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent mainIntent;
                mainIntent = new Intent(MainActivity.this, ScanActivity.class);

                startActivity(mainIntent);
                finish();
            }

        }, SPLASH_DISPLAY_LENGHT);
    }

    private void init() {


        BluetoothManager btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter btAdapter = btManager.getAdapter();
        btAdapter.enable();
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("Broadcast", "received " + intent.getStringExtra("Content"));
        }
    };
//
//    private void databaseOperation() {
//        RecordDatabase mUserDatabase = Room.databaseBuilder(getApplicationContext(), RecordDatabase.class, "users").build();
//        recordDao mUserDao = mUserDatabase.getUserDao();
//
//        //写数据库
//        Log.d(TAG, "开始写入数据...");
//        writeDatabase(mUserDao, "张三", 18);
//        writeDatabase(mUserDao, "李四", 19);
//        Log.d(TAG, "写入数据库完毕.");
//
//        //读数据库
//        Log.d(TAG, "第1次读数据库");
//        readDatabase(mUserDao);
//
//        //更新数据库
//        updateUser(mUserDao);
//
//        //读数据库
//        Log.d(TAG, "第2次读数据库");
//        readDatabase(mUserDao);
//
//        //删除数据，根据主键id
//        deleteUser(mUserDao, 1);
//
//        //读数据库
//        Log.d(TAG, "第3次读数据库");
//        readDatabase(mUserDao);
//
//        Log.d(TAG, "========================");
//        Log.d(TAG, "本轮数据库操作事务全部结束");
//        Log.d(TAG, "========================");
//    }
//
//    private void readDatabase(recordDao dao) {
//        Log.d(TAG, "读数据库...");
//        List<Record> users = dao.getAllUsers();
//        for (Record u : users) {
//            Log.d(TAG, u.mac + "," + u.name + "," + u.age + "," + u.updateTime);
//        }
//        Log.d(TAG, "读数据库完毕.");
//    }
//
//    private void writeDatabase(recordDao dao, String name, int age) {
//        Record user = new Record();
//        user.name = name;
//        user.age = age;
//        user.updateTime = System.currentTimeMillis();
//        dao.insertUser(user);
//    }
//
//    private void updateUser(recordDao dao) {
//        Log.d(TAG, "更新数据库...");
//        Record u = new Record();
//        u.mac = 2;
//        u.name = "赵五";
//        u.age = 20;
//        u.updateTime = System.currentTimeMillis();
//        dao.updateUser(u);
//        Log.d(TAG, "更新数据库完毕.");
//    }
//
//    private void deleteUser(recordDao dao, int id) {
//        Log.d(TAG, "删除数据库...");
//        Record u = new Record();
//        u.mac = id;
//        dao.deleteUser(u);
//        Log.d(TAG, "删除数据库完毕.");
//    }

}
